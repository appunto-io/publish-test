(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      articles: articles,
    };

    /////////////////////////////

    function articles(attributes) {

      const providers = require('../../index').get();
      const attributeCRUD = providers.attribute.crud;
      const types = attributeCRUD.getTypes();
      const _tmp = {};

      const _promise = new Promise(function(resolve, reject) {
        attributes.forEach(function(attribute) {
          _tmp[attribute.type] = true;
        });

        const typesArray = Object.keys(types).map(i => types[i]);
        typesArray.forEach(function(type) {
          if (!_tmp[type]) {
            reject('Attribute missing.');
          }
        });

        resolve(attributes.map(i => i._id));
      });

      return _promise;
    }
  }


})();
