(function() {

  'use strict';

  const schema = require('./schema');

  module.exports = getModel;

  /////////////////////////

  function getModel(db) {
    return db.model('Attribute', schema);
  }

})();
