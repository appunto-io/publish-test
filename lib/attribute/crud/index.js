(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {

      create: create,
      get: get,
      getById: getById,

      getTypes: getTypes,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {
      return model.build(body)
        .then(function(_attribute) {
          return save(_attribute);
        });
    }

    function get(data) {

      const _populate = {};

      _populate.collection =  (data.populate || {}).collection || '';
      _populate.fields =  (data.populate || {}).fields || null;
      _populate.matchs =  (data.populate || {}).matchs || null;
      _populate.options =  (data.populate || {}).options || null;

      return model.find(data.query)
        .limit(data.limit)
        .skip(data.skip)
        .sort(data.sort)
        .populate(_populate.collection, _populate.fields, _populate.matchs, _populate.options)
        .exec();
    }

    function getById(id) {
      return new Promise(function(resolve, reject) {

        model.findById(id)
          .exec(function(err, attribute) {
            if (err) { reject(err); }
            else if (!attribute) { reject('no attribute for ' + id ); }
            else { resolve(attribute); }
          });

      });
    }

    function getTypes() {
      return {
        // CLOTHING_SIZE: 1,
        // SHOE_SIZE: 2,
        COLOR: 3,
        // SEASONS: 4,
        CLOTHING_MATERIAL: 5,
        BRAND: 6,
      }
    }

    ///////////

    function save(attribute, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          attribute.markModified(markModified);
        }

        attribute.save(function(err, attribute) {
          if ( err ) {
            return reject( err );
          }
          resolve(attribute);
        });
      });

      return promise;
    }
  }


})();
