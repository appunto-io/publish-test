(function() {

  'use strict'
  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  // TYPE
  const CLOTHING_SIZE = 1;
  const SHOE_SIZE = 2;
  const COLOR = 3;
  const SEASONS = 4;
  const CLOTHING_MATERIAL = 5;
  const BRAND = 6;

  const AttributeSchema = new Schema({
    name: {type: String, required: true},
    oldId: Number,
    type: {
      type: Number,
      enum: [CLOTHING_SIZE, SHOE_SIZE, COLOR, SEASONS, CLOTHING_MATERIAL, BRAND],
      required: true
  	},

    updatedAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    deletedAt: { type: Date }
  }, { collection: 'attributes' });

  AttributeSchema.pre('save', function (next) {
    const now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
      this.createdAt = now;
    }
    next();
  });

  const fields = [
    {name: 'name', id: false},
    {name: 'type', id: false}
  ];

  function _validator(body) {
    const promise = new Promise(function(resolve, reject) {

      let attribute = {};

      fields.forEach(function(field) {
        if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
          return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
        } else if (!body[field.name]) {
           return reject(field.name + ' field is missing');
        } else {
          attribute[field.name] = body[field.name];
        }
      });

      resolve(attribute);
    });

    return promise;
  }

  AttributeSchema.statics.build = function(body) {

    const Attribute = this.model('Attribute');
    const promise = new Promise(function(resolve, reject) {

      _validator(body)
      .then(function(_attribute) {

        const attribute = new Attribute(_attribute);
        resolve(attribute);

      })
      .catch(reject);

    });

    return promise;
  };

  module.exports = AttributeSchema;
})()
