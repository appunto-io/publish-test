(function() {

  'use strict';

  module.exports = factory;

  function factory(mangopay) {

    const expose = {
      create: create
    };

    function create(user) {

      const promise = new Promise(function(resolve, reject) {
        mangopay.wallet.create({
          Owners: [user.Id],
          Currency: 'EUR'
        }, (err, wallet) => {
          if (err) { return reject(err); }
          return resolve(wallet);
        });
      });

      return promise;

    }

    return expose;

  }

})();
