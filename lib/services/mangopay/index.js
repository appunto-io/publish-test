(function() {

  'use strict';

  module.exports = factory;

  function factory(config) {

    const mangopay = require('mangopay')({
      username: config.mangopay.clientId,
      password: config.mangopay.apiKey,
      production: config.mangopay.clientId === 'dresswing'
    });

    const bank = require('./bank')(mangopay);
    const card = require('./card')(config, mangopay);
    const user = require('./user')(mangopay);
    const wallet = require('./wallet')(mangopay);

    const expose = {
      bank,
      card,
      user,
      wallet,
    };

    return expose;

  }

})();
