(function() {

  'use strict';

  module.exports = factory;

  function factory(config, mangopay) {

    const expose = {
      create: create,
      list: list,
      remove: remove
    };

    function create(data) {

      const promise = new Promise(function(resolve, reject) {

        const { user, card } = data ;

        mangopay.card.initRegistration({
          UserId: user.Id,
          Currency: 'EUR'
        }, (_err, registration) => {
           // after obtaining a cardRegistration object
           // we send the card details to the PSP (payline) url
          const cardDetails = {
            data: registration.PreregistrationData,
            accessKeyRef: registration.AccessKey,
            accessKey: registration.AccessKey,
            cardNumber: card.number,
            CardType: card.type,
            cardExpirationDate: card.expirationDate,
            cardCvx: card.cvx
          };
          mangopay.card.sendCardDetails(registration, cardDetails, (err, d) => {
            if (err) { return reject(err); }

            return mangopay.card.preAuthorization({
              AuthorId: user.Id,
              DebitedFunds: {
                Currency: 'EUR',
                Amount: 1 * 100
              },
              SecureMode: 'DEFAULT',
              SecureModeNeeded: false,
              CardId: d.CardId,
              SecureModeReturnURL: config.api,
            }, (_err, dPreAuthorization) => {
               // dPreAuthorization
               if (_err) { return reject(_err); }
               if (dPreAuthorization.Status === 'SUCCEEDED') {
                 return resolve(d);
               } else {
                 return resolve(false);
               }
               return reject(_err);
             });
         });

      });

      });

      return promise;

    }

    function list(user) {
      const promise = new Promise(function(resolve, reject) {

        mangopay.user.cards({
          UserId: user.Id,
        }, (err, result) => {
          if (err) { return reject(err); }
          const _result = _converterListCard(result);
          return resolve(_result);
        });

      });

      return promise;
    }

    function remove(data) {

      const promise = new Promise(function(resolve, reject) {

        const { user, card: { Id: CardId } } = data;

        mangopay.card.update({
          UserId: user.Id,
          Id: CardId,
          Active: false
        }, (err, result) => {
          if (err) { return reject(err); }
          return resolve({});
        });

      });

      return promise;

    }

    function _converterListCard(list) {
      const result = [];

      list.forEach((elem) => {
        if (elem.Active) {
          result.push({
            Id: elem.Id,
            Alias: elem.Alias,
            ExpirationDate: elem.ExpirationDate,
            CardType: elem.CardType
          });
        }
      });

      return result;
    }


    return expose;

  }

})();
