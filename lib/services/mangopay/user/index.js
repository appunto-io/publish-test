(function() {

  'use strict';

  module.exports = factory;

  function factory(mongopay) {

    const expose = {
      create: create
    };

    Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };

    function create(data) {

      const promise = new Promise(function(resolve, reject) {
        mongopay.user.create({
          FirstName: data.firstName || '-',
          LastName: data.lastName || '-',
          Birthday: new Date('01-01-90').getUnixTime(),
          Nationality: 'FR',
          CountryOfResidence: 'FR',
          PersonType: 'NATURAL',
          Email: data.email,
          Tag: data._id,
        }, (err, user) => {
          if (err) { return reject(err); }
          return resolve(user);
        });
      });

      return promise;

    }

    return expose;

  }

})();
