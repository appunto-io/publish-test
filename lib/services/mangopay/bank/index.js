(function() {

  'use strict';

  module.exports = factory;

  function factory(mangopay) {

    const expose = {
      create: create,
      list: list,
      remove: remove
    };

    function create(data) {

      const promise = new Promise(function(resolve, reject) {

        const { user, bank } = data ;

        mangopay.bank.create({
          UserId: user.Id,
          Type: 'IBAN',
          OwnerName: bank.name, // Required
          OwnerAddress: bank.address, // Required
          IBAN: bank.iban, // Required
          BIC: bank.bic // Required
        }, (err, result) => {
          if (err) { return reject(err); }
          return resolve({});
        });

      });

      return promise;

    }

    function list(user) {
      const promise = new Promise(function(resolve, reject) {

        mangopay.user.banks({
          UserId: user.Id,
        }, (err, result) => {
          if (err) { return reject(err); }
          const _result = _converterListBank(result);
          return resolve(_result);
        });

      });

      return promise;
    }

    function remove(data) {

      const promise = new Promise(function(resolve, reject) {

        const { user, bank: { Id: BankId } } = data;

        mangopay.bank.update({
          UserId: user.Id,
          BankId: BankId,
          Active: false
        }, (err, result) => {
          if (err) { return reject(err); }
          return resolve({});
        });

      });

      return promise;

    }

    function _converterListBank(list) {
      const result = [];

      list.forEach((elem) => {
        if (elem.Active) {
          result.push({
            OwnerName: elem.OwnerName,
            OwnerAddress: elem.OwnerAddress,
            Id: elem.Id,
            IBAN: elem.IBAN,
            BIC: elem.BIC
          });
        }
      });

      return result;
    }

    return expose;

  }

})();
