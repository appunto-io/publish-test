(function() {

  'use strict';

  const AWS = require('aws-sdk');
  const fs = require('fs');

  module.exports = factory;

  function factory(config) {

    AWS.config.update(config.aws);

    const s3 = new AWS.S3();

    const expose = {
      put: put,
      delete: remove
    };

    function put(folder, file) {

      const promise = new Promise(function(resolve, reject) {

        const buffer = fs.readFileSync(file.path);
        const params = {
          Bucket: config.aws.bucket,
          ACL: 'public-read',
          Key: folder + file.name,
          Body: buffer,
          ContentType: file.mimetype
        };

        const putObjectCB = function(err, data) {
          if (err) {
            reject(err);
          } else {
            fs.unlink(file.path, (err) => {
              if (err) {
                reject(err);
              } else {
                resolve(data);
              }
            });
          }
        };

        s3.upload(params, putObjectCB);

      });

      return promise;

    }

    function remove(path) {

      const promise = new Promise(function(resolve) {

        const params = {
          Bucket: config.aws.bucket,
          Key: path
        };

        const _callback = function() {
          resolve();
        };

        s3.deleteObject(params, _callback);

      });

      return promise;

    }

    return expose;

  }

})();
