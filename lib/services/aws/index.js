(function() {

  'use strict';

  module.exports = factory;

  function factory(config) {

    const s3 = require('./s3')(config);

    const expose = {
      s3,
    };

    return expose;

  }

})();
