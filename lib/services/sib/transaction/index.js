(function() {

  'use strict';

  const request = require('request');

  module.exports = factory;

  function factory(config) {

    const expose = {
      send: send
    };

    function send(data) {

      const promise = new Promise(function(resolve, reject) {
        var options = {
          method: 'POST',
          headers: {
             'Content-Type' : 'application/json',
             'api-key': config.sib.apiKeyV3
          },
          body: {
            emailTo: [ data.user.email ],
            attributes: data.template.body
          },
          json: true,
          url: 'https://api.sendinblue.com/v3/smtp/templates/' + data.template.id + '/send'
        };

        request(options, function (error) {
          if (error) {
            reject(error);
          }
          else {
            resolve(data.user);
          }
        });
      });

      return promise;

    }

    return expose;

  }

})();
