(function() {

  'use strict';

  module.exports = factory;

  function factory(config) {

    const list = require('./list')(config);
    const contact = require('./contact')(config);
    const transaction = require('./transaction')(config);

    const expose = {
      list,
      contact,
      transaction,
    };

    return expose;

  }

})();
