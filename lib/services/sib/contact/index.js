(function() {

  'use strict';

  const request = require('request');

  module.exports = factory;

  function factory(config) {

    const expose = {
      create: create
    };

    function create(user, listId) {

      const promise = new Promise(function(resolve, reject) {

        const options = {
          method: 'POST',
          json: true,
          url: 'https://api.sendinblue.com/v3/contacts',
          headers: {
             'Content-Type' : 'application/json',
             'api-key': config.sib.apiKeyV3
          },
          body: {
            listIds: [ listId ],
            email: user.email,
            attributes: {
              PRENOM: user.firstName,
              NOM: user.lastName,
            },
            emailBlacklisted: false,
            smsBlacklisted: false,
            updateEnabled: true
          },
        };

        request(options, function (error) {
          if (error) {
            reject(error);
          } else {
            resolve(user);
          }
        });
      });

      return promise;
    }

    return expose;

  }

})();
