(function() {

  'use strict';

  const request = require('request');

  module.exports = factory;

  function factory(config) {

    const expose = {
      addContact: addContact,
      removeContact: removeContact
    };

    function addContact(user, listId) {

      const promise = new Promise(function(resolve, reject) {
        var options = {
          method: 'POST',
          headers: {
             'Content-Type' : 'application/json',
             'api-key': config.sib.apiKeyV3
          },
          body: { emails: [ user.email ] },
          json: true,
          url: 'https://api.sendinblue.com/v3/contacts/lists/' + listId + '/contacts/add'
        };

        request(options, function (error) {
          if (error) {
            reject(error);
          }
          else {
            resolve(user);
          }
        });
      });

      return promise;

    }

    function removeContact(user, listId) {

      const promise = new Promise(function(resolve, reject) {

        var options = {
          method: 'POST',
          headers: {
             'Content-Type' : 'application/json',
             'api-key': config.sib.apiKeyV3
          },
          body: { emails: [ user.email ] },
          json: true,
          url: 'https://api.sendinblue.com/v3/contacts/lists/' + listId + '/contacts/remove'
        };

        request(options, function (error) {
          if (error) {
            reject(error);
          } else {
            resolve(user);
          }
        });
      });

      return promise;
    }


    return expose;

  }

})();
