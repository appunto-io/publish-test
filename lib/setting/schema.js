const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SettingSchema = new Schema({
  // Proprietaire
  excludedMail: [String],
  excludedIP: [String],

  updatedAt: { type: Date, default: Date.now },
  createdAt: Date,
  deletedAt: { type: Date }
}, { collection: 'settings' });

SettingSchema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = SettingSchema;
