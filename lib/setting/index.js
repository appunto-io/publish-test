(function() {
  'use strict';

  module.exports = factory;

  /////////////////////////

  function factory(dbConnection) {

    const model = require('./model')(dbConnection);

    // const converter = require('./converter')();

    // const auth = require('./auth')();
    // const crud = require('./crud')(model);
    // const fields = require('./fields')();
    // const moodze = require('./moodze')();

    const oauthProvider = {
      model: model
    };

    return oauthProvider;
  }

})();
