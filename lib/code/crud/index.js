(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      create: create,
      getCode: getCode,

      remove: remove,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {
      return model.build(body)
        .then(function(_code) {
          return save(_code);
        });
    }

    function getCode(query) {
      const promise = new Promise(function(resolve, reject) {
        model.findOne(query)
          .exec(function(err, code) {
            if (err) { reject(err); }
            else { resolve(code); }
          });
      });
      return promise;
    }

    function remove(query) {
      const promise = new Promise(function(resolve, reject) {
          model.remove(query)
            .exec(function(err) {
              if (err) { reject(err); }
              else { resolve(); }
            });
      });

      return promise;
    }

    ///////////

    function save(code, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          code.markModified(markModified);
        }

        code.save(function(err, code) {
          if ( err ) {
            return reject( err );
          }
          resolve(code);
        });
      });

      return promise;
    }
  }


})();
