(function() {
 'use strict'

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;
  const ObjectId = Schema.ObjectId;

  const CodeSchema = new Schema({
    code: { type: Number, required: true },
    email: { type: String, required: true },
    userId: { type: ObjectId, required: true },
    oldId: Number,
    updatedAt: { type: Date, default: Date.now },
    createdAt: Date,
  }, { collection: 'codes' });

  const fields = [
    {name: 'email', id: false},
    {name: 'userId', id: true}
  ];

  function _validator(body) {
    const promise = new Promise(function(resolve, reject) {

      let code = {};

      fields.forEach(function(field) {
        if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
          return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
        } else if (!body[field.name]) {
           return reject(field.name + ' field is missing');
        } else {
          code[field.name] = body[field.name];
        }
      });

      code.code = Number(Math.random() * (999999 - 100000) + 100000).toFixed();

      resolve(code);
    });

    return promise;
  }

  CodeSchema.statics.build = function(body) {

    const Code = this.model('Code');
    const promise = new Promise(function(resolve, reject) {

      _validator(body)
      .then(function(_code) {

        const code = new Code(_code);
        resolve(code);

      })
      .catch(reject);

    });

    return promise;
  };

  CodeSchema.pre('save', function (next) {
    const now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
      this.createdAt = now;
    }
    next();
  });

  module.exports = CodeSchema;

})();
