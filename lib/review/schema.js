const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const ReviewSchema = new Schema({

  reviewer: { type: ObjectId, ref: 'User', required: true },
  owner: { type: ObjectId, ref: 'User', required: true },

  comment: { String },

  grade: {
    experience: { Number },
    communication: { Number },
    conformity: { Number },
    global: { Number }
  },

  createdAt: Date,
  updatedAt: Date,

}, { collection: 'reviews' });

ReviewSchema.pre('save', function (next) {
  return (async () => {
    const now = new Date();

    this.grade.global = ( this.grade.experience + this.grade.communication + this.grade.conformity ) / 3;

    this.updatedAt = now;
    if (!this.createdAt) {
      // emitter.emit('user:new', { row: this });
      this.createdAt = now;
    }
    next();
  })();
});

module.exports = ReviewSchema;
