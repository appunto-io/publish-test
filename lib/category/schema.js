(function() {
  'use strict';

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  const CategorySchema = new Schema({

    name: String,
    description: String,

    parent: { type: Schema.ObjectId, ref: 'Category' },

    updatedAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    deletedAt: { type: Date }

  }, { collection: 'categories' });

  CategorySchema.pre('save', function (next) {
    const now = new Date();
    this.updatedAt = now;
    if (!this.createdAt) {
      this.createdAt = now;
    }
    next();
  });

  const fields = [
    {name: 'name', id: false},
    {name: 'description', id: false}
  ];

  function _validator(body) {
    const promise = new Promise(function(resolve, reject) {

      let category = {};

      fields.forEach(function(field) {
        if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
          return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
        } else if (!body[field.name]) {
           return reject(field.name + ' field is missing');
        } else {
          category[field.name] = body[field.name];
        }
      });

      resolve(category);
    });

    return promise;
  }

  CategorySchema.statics.build = function(body) {

    const Category = this.model('Category');
    const promise = new Promise(function(resolve, reject) {

      _validator(body)
      .then(function(_attribute) {

        const category = new Category(_attribute);
        resolve(category);

      })
      .catch(reject);

    });

    return promise;
  };

  CategorySchema.pre('save', function (next) {
    this.updatedAt = new Date();
    next();
  });

  module.exports = CategorySchema;

})();
