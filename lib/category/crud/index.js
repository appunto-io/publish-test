(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      create: create,
      get: get,
      getById: getById,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {
      return model.build(body)
        .then(function(_category) {
          return save(_category);
        });
    }

    function get(data) {

      const _populate = {};

      _populate.collection =  (data.populate || {}).collection || '';
      _populate.fields =  (data.populate || {}).fields || null;
      _populate.matchs =  (data.populate || {}).matchs || null;
      _populate.options =  (data.populate || {}).options || null;

      return model.find(data.query)
        .limit(data.limit)
        .skip(data.skip)
        .sort(data.sort)
        .populate(_populate.collection, _populate.fields, _populate.matchs, _populate.options)
        .exec();
    }

    function getById(id) {
      return new Promise(function(resolve, reject) {

        model.findById(id)
          .exec(function(err, category) {
            if (err) { reject(err); }
            else if (!category) { reject('no category for ' + id ); }
            else { resolve(category); }
          });

      });
    }

    ///////////

    function save(category, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          category.markModified(markModified);
        }

        category.save(function(err, category) {
          if ( err ) {
            return reject( err );
          }
          resolve(category);
        });
      });

      return promise;
    }
  }


})();
