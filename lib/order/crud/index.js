(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      create: create,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {
      return model.build(body)
        .then(function(order) {
          return save(order);
        });
    }

    ///////////

    function save(order, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          order.markModified(markModified);
        }

        order.save(function(err, order) {
          if ( err ) {
            return reject( err );
          }
          resolve(order);
        });
      });

      return promise;
    }
  }


})();
