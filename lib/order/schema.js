const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const OrderSchema = new Schema({

  oldId: Number,

  // price article
  amount: Number,

  // Pressing
  cleaningPrice: { type: Number, default: 0 },

  // Comission dresswing
  fee: { type: Number, default: 20 },
  cardId: {},

  // Total amount
  totalAmount: Number,

  // Locataire
  hirer: { type: ObjectId, ref: 'User' },

  // Project
  article: { type: ObjectId, ref: 'Article' },

  // Proprietaire
  owner: { type: ObjectId, ref: 'User' },

  start: { type: Date },
  end: { type: Date },
  delay: { type: Number },

  acceptedAt: { type: Date },
  levyAt: { type: Date },
  isPaid: { type: Boolean, default: false },
  paidAt: { type: Date },

  // 101 Paiement effectué et cagnotte créditée
  // 100 En attente
  // 99 Erreur de paiement
  paymentState: { type: Number, default: 100 },

  paymentStateMessage: String,

  // 202 Refused
  // 201 Accepted
  // 200 Waiting validation
  // 199 Canceled
  state: { type: Number, default: 200 },

  // 100 enabled
  // 101 disabled
  // 99 archived
  status: { type: Number, default: 100 },

  isConnected: { type: Boolean, default: true },

  // Gestion des relances
  hasRelanced: { type: Boolean, default: false },
  // Gestion des rappels
  hasCallback: { type: Boolean, default: false },

  updatedAt: { type: Date, default: Date.now },
  createdAt: { type: Date, default: Date.now },
  deletedAt: { type: Date }
}, { collection: 'orders' });

const fields = [
  {name: 'owner', id: true},
  {name: 'hirer', id: true},
  {name: 'article', id: true},
  {name: 'cardId', id: false},
  {name: 'start', id: false},
  {name: 'end', id: false},
  {name: 'delay', id: false},
  {name: 'fee', id: false},
  {name: 'totalAmount', id: false},
];

function _validator(body) {
  const promise = new Promise(function(resolve, reject) {

    let order = {};

    fields.forEach(function(field) {
      if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
        return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
      } else if (!body[field.name]) {
         return reject(field.name + ' field is missing');
      } else {
        order[field.name] = body[field.name];
      }
    });

    resolve(order);
  });

  return promise;
}

OrderSchema.statics.build = function(body) {

  const Order = this.model('Order');
  const promise = new Promise(function(resolve, reject) {

    _validator(body)
    .then(function(_order) {

      const order = new Order(_order);
      resolve(order);

    })
    .catch(reject);

  });

  return promise;
};

OrderSchema.pre('save', function (next) {
  return (async () => {
    const now = new Date();

    // Change updatedAt
    this.updatedAt = now;

    // Change delay
    this.end = moment(this.start || new Date()).add(this.delay || 0, 'days').subtract(1, 'hours').toISOString();
    if (!this.delay && !!this.start && this.end) {
      this.delay = moment(moment(this.end).format('YYYY-MM-DD')).diff(moment(this.start).format('YYYY-MM-DD'), 'days');
    }

    // If no createdAt, change it
    if (!this.createdAt) {
      this.createdAt = now;
      // emitter.emit('order:new', { row: this });
    }

    next();
  })();
});

module.exports = OrderSchema;
