const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const WishSchema = new Schema({
  // Proprietaire
  user: { type: ObjectId, ref: 'User' },
  article: { type: ObjectId, ref: 'Article' },

  oldId: Number,
  userOldId: Number,
  oldArticleId: Number,

  updatedAt: { type: Date, default: Date.now },
  createdAt: Date,
  deletedAt: { type: Date }
}, { collection: 'wishs' });

WishSchema.pre('save', function (next) {
  const now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = WishSchema;
