(function() {

  'use strict'

  const Hashes = require('jshashes');

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  const UserSchema = new Schema({
    username: String,
    oldId: Number,
    email: {
  		type: String,
  		lowercase: true,
  		trim: true,
  		unique: true,
  		match: [/.+\@.+\..+/, 'Please fill a valid email address']
  	},

    roles: {
  		type: [{
  			type: String,
  			enum: ['user', 'partner', 'admin']
  		}],
  		default: ['user']
  	},

    salt: String,
    password: String,
    lastLogin: Date,
    confirmationToken: String,

    firstName: String,
    lastName: String,
    mobile: String,
    birthday: Date,
    height: String,

    avatar: String,
    identity: String,
    description: String,
    balance: Number,
    amountToTransfer: Number,

    isEmailVerified: { type: Boolean, default: false },
    isMobileVerified: { type: Boolean, default: false },
    isIdentityVerified: Boolean,
    isEmailSent: { type: Boolean, default: false },
    facebookId: String,
    facebookAccessToken: String,
    facebookLink: String,

    address: [{
      address: String,
      city: String,
      zip: String,
      country: String,
    }],

    // Stripe ID
    stripeId: String,

    // IBAN & BIC
    iban: String,
    bic: String,
    accountName: String,

    // Manage notification
    notification: { type: Boolean, default: true },
    touchId: { type: Boolean, default: false },
    deviceToken: String,
    deviceOS: String,

    mangopay: Object,

    // 99 archived
    // 100 enabled
    // 101 disabled
    status: { type: Number, default: 100 },

    isConnected: { type: Boolean, default: true },
    updatedAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    deletedAt: { type: Date }
  }, { collection: 'users' });

  const fields = [
    {name: 'email', id: false},
    {name: 'password', id: false}
  ];

  function _validator(body) {
    const promise = new Promise(function(resolve, reject) {

      let user = {};

      fields.forEach(function(field) {
        if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
          return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
        } else if (!body[field.name]) {
           return reject(field.name + ' field is missing');
        } else {
          user[field.name] = body[field.name];
        }
      });

      user.password = new Hashes.SHA256().hex(user.password);
      user.status = 100;

      resolve(user);
    });

    return promise;
  }

  UserSchema.statics.build = function(body) {

    const User = this.model('User');
    const promise = new Promise(function(resolve, reject) {

      _validator(body)
      .then(function(_user) {

        const user = new User(_user);
        resolve(user);

      })
      .catch(reject);

    });

    return promise;
  };

  UserSchema.statics.passwordChange = function(user, password) {
    user.password = new Hashes.SHA256().hex(password);
    return Promise.resolve(user);
  };

  // UserSchema.pre('save', function (next) {
  //     this.updatedAt = now;
  //     if (!this.createdAt) {
  //       // emitter.emit('user:new', { row: this });
  //       this.createdAt = now;
  //     }
  //     next();
  //   })();
  // });

  module.exports = UserSchema;

})();
