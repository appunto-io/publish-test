(function() {
  'use strict';

  module.exports = factory;

  /////////////////////////

  function factory(dbConnection) {

    const model = require('./model')(dbConnection);

    const converter = require('./converter')();

    const crud = require('./crud')(model);
    const password = require('./password')(model);

    const userProvider = {
      model: model,

      crud: crud,
      password: password,

      converter: converter
    };

    return userProvider;
  }

})();
