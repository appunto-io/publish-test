(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      change: change,
    };

    /////////////////////////////

    function change(user, password) {
      const providers = require('../../index').get();
      const UserCRUD = providers.user.crud;
      return model.passwordChange(user, password)
        .then(function(_user) {
          return UserCRUD.save(_user);
        });
    }

  }


})();
