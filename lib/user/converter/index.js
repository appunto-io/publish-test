'use strict';

module.exports = factory;

function factory() {

  const expose = {
    userPublic: userPublic,
    userPrivate: userPrivate,
  };

  ///////////////////////////

  function userPublic(user) {

    return {
      firstName: user.firstName,
      lastName: user.lastName,
      created: user.createdAt,
      birthday: user.birthday,
    };

  }

  function userPrivate(user) {

    return {
      firstName: user.firstName,
      lastName: user.lastName,
      address: user.address || [],
      isEmailVerified: user.isEmailVerified,
      created: user.createdAt,
      birthday: user.birthday,
      email: user.email,
      mobile: user.mobile,
      height: user.height,
      _id: user._id
    };

  }

  return expose;
}
