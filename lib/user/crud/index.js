(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      create: create,
      get: get,
      getById: getById,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {
      return model.build(body)
        .then(function(_user) {
          return save(_user);
        });
    }

    function get(data) {

      const _populate = {};

      _populate.collection =  (data.populate || {}).collection || '';
      _populate.fields =  (data.populate || {}).fields || null;
      _populate.matchs =  (data.populate || {}).matchs || null;
      _populate.options =  (data.populate || {}).options || null;

      return model.find(data.query)
        .limit(data.limit)
        .skip(data.skip)
        .sort(data.sort)
        .populate(_populate.collection, _populate.fields, _populate.matchs, _populate.options)
        .exec();
    }

    function getById(id) {
      return new Promise(function(resolve, reject) {

        model.findById(id)
          .exec(function(err, user) {
            if (err) { reject(err); }
            else if (!user) { reject('no user for ' + id ); }
            else { resolve(user); }
          });

      });
    }
    ///////////

    function save(user, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          user.markModified(markModified);
        }

        user.save(function(err, user) {
          if ( err ) {
            return reject( err );
          }
          resolve(user);
        });
      });

      return promise;
    }
  }


})();
