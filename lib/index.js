(function() {

  'use strict';

  module.exports.init = singleton;
  module.exports.get = get;

  let providers;

  const article = require('./article');
  const attribute = require('./attribute');
  const category = require('./category');
  const code = require('./code');
  const dressing = require('./dressing');
  const order = require('./order');
  // const review = require('./review');
  // const setting = require('./review');
  const user = require('./user');
  // const wish = require('./wish');

  //////////////////////

  const sib = require('./services/sib');
  const aws = require('./services/aws');
  const mangopay = require('./services/mangopay');

  //////////////////////

  function singleton(dependencies, config) {

    const db = dependencies.db;

    providers = {
      article : article(db),
      attribute : attribute(db),
      category : category(db),
      code : code(db),
      dressing : dressing(db),
      user : user(db),
      order : order(db),
      // review : review(db),
      // setting : setting(db),
      // wish : wish(db),

      //////////////

      sib: sib(config),
      mangopay: mangopay(config),
      aws: aws(config)
    };

    return providers;
  }

  function get() {
    return providers;
  }

})();
