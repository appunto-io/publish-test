(function() {
  'use strict';

  module.exports = factory;

  function factory(model) {

    return {
      create: create,
      get: get,
      getById: getById,
      addPhoto: addPhoto,

      update: update,

      //
      save: save
    };

    /////////////////////////////

    function create(body) {

      const providers = require('../../index').get();

      const attributeCRUD = providers.attribute.crud;
      const attributesValidator = providers.attribute.validator;

      let article;

      return attributeCRUD.get({
          query: {
            _id: {
              $in: body.attributes
            }
          }
        })
        .then(function(attributes) {
          return attributesValidator.articles(attributes);
        })
        .then(function(attributes) {
          body.attributes = attributes;
          return model.build(body);
        })
        .then(function(_article) {

          article = _article;

          body.attributes.forEach(function(elem) {
            return article.attributes.push(elem);
          });

          const files = body.photos.map(function (photo) {
            const data = {
              folder: 'articles/' + article._id + '/',
              file: {
                name: photo.filename + '.jpg',
                mimetype: photo.mimetype,
                path: photo.path
              }
            };
            return pushToCloud(data);
          });
          return Promise.all(files);

        })
        .then(function(files) {
          article.photos = files.map(i => i.url);
          return save(article);
        });

    }

    function update(data) {

      const _ = require('lodash');

      const providers = require('../../index').get();

      const attributeCRUD = providers.attribute.crud;
      const attributesValidator = providers.attribute.validator;

      let article = data.article;
      let body =  data.body;
      let deletePictures =  data.deletePictures;
      let files =  data.files;

      let promessDeleteCloud = [];
      let promessAddCloud = [];

      if (deletePictures && article.photos.length !== 1) {
        deletePictures = deletePictures.split(',');
        article.photos = article.photos.filter(photo => deletePictures.indexOf(photo) === -1);
        promessDeleteCloud = deletePictures.map(function(photo) {
          return deletePhoto(photo);
        });
      }

      if (files && files.photos) {
        promessAddCloud = files.photos.map(function(photo) {
          return addPhoto({photo, article: article});
        });
      }

      return attributeCRUD.get({
          query: {
            _id: {
              $in: body.attributes
            }
          }
        })
        .then(function(attributes) {
          return attributesValidator.articles(attributes);
        })
        .then(function(attributes) {
          body.attributes = attributes;Promise.all(promessAddCloud);
          return Promise.all(promessAddCloud);
        })
        .then(function(_files) {
          if (_files && _files.length) {
            article.photos = article.photos.concat(_files.map(i => i.url));
          }
          return Promise.all(promessDeleteCloud);
        })
        .then(function() {

          // PUT ARTICLE IN WAITING
          article.statusValidation = 100;

          article = _.extend(article, body);
          console.log(article);
          return save(article);
        });

    }

    function get(data) {

      const _populate = {};

      _populate.collection =  (data.populate || {}).collection || '';
      _populate.fields =  (data.populate || {}).fields || null;
      _populate.matchs =  (data.populate || {}).matchs || null;
      _populate.options =  (data.populate || {}).options || null;

      return model.find(data.query)
        .limit(data.limit)
        .skip(data.skip)
        .sort(data.sort)
        .populate(_populate.collection, _populate.fields, _populate.matchs, _populate.options)
        .exec();
    }

    function getById(data) {
      return new Promise(function(resolve, reject) {

        const _populate = {};

        _populate.collection =  (data.populate || {}).collection || '';
        _populate.fields =  (data.populate || {}).fields || null;
        _populate.matchs =  (data.populate || {}).matchs || null;
        _populate.options =  (data.populate || {}).options || null;

        model.findById(data.id)
          .populate(_populate.collection, _populate.fields, _populate.matchs, _populate.options)
          .exec(function(err, article) {
            if (err) { reject(err); }
            else if (!article) { reject('no article for ' + data.id ); }
            else { resolve(article); }
          });

      });
    }

    function addPhoto(body) {
      const data = {
        folder: 'articles/' + body.article._id + '/',
        file: {
          name: body.photo.filename + '.jpg',
          mimetype: body.photo.mimetype,
          path: body.photo.path
        }
      };
      return pushToCloud(data);
    }

    function deletePhoto(photo) {
      const providers = require('../../index').get();
      const s3 = providers.aws.s3;

      const start = photo.indexOf('articles');
      const end = photo.length;
      const path = photo.substr(start, end);
      return s3.delete(path);
    }

    ///////////

    function pushToCloud(data) {
      const providers = require('../../index').get();
      const s3 = providers.aws.s3;
      return s3.put(data.folder, data.file).
        then(function(response) {
          return {name: data.file.name, url: response.Location, type: data.file.mimetype};
        });
    }

    function save(article, markModified) {
      const promise = new Promise(function(resolve, reject) {
        if (markModified) {
          article.markModified(markModified);
        }

        article.save(function(err, article) {
          if ( err ) {
            return reject( err );
          }
          resolve(article);
        });
      });

      return promise;
    }
  }


})();
