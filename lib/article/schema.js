(function() {

  'use strict';

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;
  const ObjectId = Schema.ObjectId;

  const STATUS_AVAILABLE = 100;
  const STATUS_NOT_AVAILABLE = 101;

  const STATE_VALIDATION_WAITING = 100;
  const STATE_VALIDATION_VALIDATE = 101;
  const STATE_VALIDATION_REJECT = 102;

  const STATE_GOOD = 100;
  const STATE_EXCELLENT = 101;
  const STATE_NEW = 102;

  const STATUS_VALIDATION_ARCHIVED = 99;
  const STATUS_VALIDATION_ENABLED = 100;
  const STATUS_VALIDATION_DISABLED = 101;

  const ArticleSchema = new Schema({

    name: {type: String, required: true},
    description: String,

    user: { type: ObjectId, ref: 'User', required: true },

    // oldId: Number,

    attributes: [{ type: ObjectId, ref: 'Attribute' }],
    category: { type: ObjectId, ref: 'Category' },


    // Is vintage
    vintage: { type: Boolean, default: false },

    // All photo
    photos: [String],

    // Photo originale
    originalPhotos: [{type: String}],

    // Photo pat default
    defaultPhoto: String,

    // Prix d'achat
    priceBuy: { type: Number, default: 0 },
    price4days: { type: Number, required: true },
    price8days: { type: Number, required: true },

    totalPrice4days: { type: Number },
    totalPrice8days: { type: Number },

    // Prix du pressing
    pricePressing: { type: Number, default: 0 },

    // Année d'achat
    yearSale: { type: Number, default: false },

    ///////////////////////////////////////////////////
    // ADMIN

    // Prix de vente
    priceSale: { type: Number, default: 0 },

    // Prix argus
    priceArgus: { type: Number, default: 0 },

    // Prix de caution
    priceCaution: { type: Number, default: 0 },

    optionOfSale: { type: Boolean, default: false },

    selected: { type: Boolean, default: false },

    //////////////////////////////////////////////////

    status: {
      type: Number,
      enum: [STATUS_AVAILABLE, STATUS_NOT_AVAILABLE],
      default: STATUS_AVAILABLE
    },

    stateValidation: {
      type: Number,
      enum: [STATE_VALIDATION_WAITING, STATE_VALIDATION_VALIDATE, STATE_VALIDATION_REJECT],
      default: STATE_VALIDATION_WAITING
    },

    validationAt: { type: Date },

    state: {
      type: Number,
      enum: [STATE_GOOD, STATE_EXCELLENT, STATE_NEW],
      default: STATE_GOOD,
    },

    // zip: { type: String },

    statusValidation: {
      type: Number,
      enum: [STATUS_VALIDATION_ARCHIVED, STATUS_VALIDATION_ENABLED, STATUS_VALIDATION_DISABLED],
      default: STATUS_VALIDATION_ENABLED
    },

    // totalPrice4days: { type: Number, default: 0 },
    // totalPrice8days: { type: Number, default: 0 },

    updatedAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },

    deletedAt: { type: Date }
  }, { collection: 'articles' });


  const fields = [

    {name: 'category', id: true},
    {name: 'user', id: true},

    {name: 'name', id: false},
    {name: 'vintage', id: false},
    {name: 'priceBuy', id: false},
    {name: 'price4days', id: false},
    {name: 'price8days', id: false},
    {name: 'pricePressing', id: false},
    {name: 'yearSale', id: false}

  ];

  function _validator(body) {
    const promise = new Promise(function(resolve, reject) {

      let article = {};

      fields.forEach(function(field) {
        if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
          return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
        } else if (!body[field.name]) {
           return reject(field.name + ' field is missing');
        } else {
          article[field.name] = body[field.name];
        }
      });

      article.totalPrice4days = article.price4days + article.pricePressing;
      article.totalPrice8days = article.price8days + article.pricePressing;

      resolve(article);
    });

    return promise;
  }

  ArticleSchema.statics.build = function(body) {

    const Article = this.model('Article');
    const promise = new Promise(function(resolve, reject) {

      _validator(body)
      .then(function(_article) {

        const article = new Article(_article);
        resolve(article);

      })
      .catch(reject);

    });

    return promise;
  };


  // ArticleSchema.pre('save', async function (next) {
  //   this.updatedAt = new Date();
  //
  //   // this.totalPrice4days = this.price4days + this.pricePressing;
  //   // this.totalPrice8days = this.price8days + this.pricePressing;
  //
  //   this.photos = this.photos || [];
  //   this.originalPhotos = this.originalPhotos || [];
  //
  //     // emitter.emit('article:new', { row: this });
  //   next();
  //
  //   }
  // );

  module.exports = ArticleSchema;

})();
