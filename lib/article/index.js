(function() {
  'use strict';

  module.exports = factory;

  /////////////////////////

  function factory(dbConnection) {
    const model = require('./model')(dbConnection);
    const crud = require('./crud')(model);

    const oauthProvider = {
      model: model,
      crud: crud
    };

    return oauthProvider;
  }

})();
