(function() {

  'use strict'
    const mongoose = require('mongoose');
    const Schema = mongoose.Schema;
    const ObjectId = Schema.ObjectId;

    const DressingSchema = new Schema({
      name: String,
      dressing: { type: ObjectId, ref: 'User' },

      oldId: Number,

      // 100 enabled
      // 101 disabled
      // 102 archived
      status: { type: Number, default: 100 },

      rateFee: { type: Number, default: 20 },

      isConnected: { type: Boolean, default: true },
      updatedAt: { type: Date, default: Date.now },
      createdAt: Date,
      deletedAt: { type: Date }
    }, { collection: 'dressings' });

    const fields = [
      {name: 'user', id: true},
      {name: 'name', id: false}
    ];

    function _validator(body) {
      const promise = new Promise(function(resolve, reject) {

        let dressing = {};

        fields.forEach(function(field) {
          if (field.id && !mongoose.Types.ObjectId.isValid(body[field.name])) {
            return reject(field.name + ' field is invalid '+ mongoose.Types.ObjectId.isValid(body[field.name]));
          } else if (!body[field.name]) {
             return reject(field.name + ' field is missing');
          } else {
            dressing[field.name] = body[field.name];
          }
        });
        resolve(dressing);
      });

      return promise;
    }

    DressingSchema.statics.build = function(body) {

      const Dressing = this.model('Dressing');
      const promise = new Promise(function(resolve, reject) {

        _validator(body)
        .then(function(_dressing) {

          const dressing = new Dressing(_dressing);
          resolve(dressing);

        })
        .catch(reject);

      });

      return promise;
    };

    DressingSchema.pre('save', function (next) {
      const now = new Date();
      this.updatedAt = now;
      if (!this.createdAt) {
        this.createdAt = now;
      }
      next();
    });

    module.exports = DressingSchema;
})();
